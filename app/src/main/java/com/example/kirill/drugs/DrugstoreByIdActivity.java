package com.example.kirill.drugs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.kirill.drugs.Entity.Drugstore;
import com.example.kirill.drugs.Storage.DrugstoresStorage;

public class DrugstoreByIdActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drugstore_by_id);

        Intent intent = getIntent();

        int Id = Integer.parseInt(intent.getStringExtra("IdDrugstore"));

        DrugstoresStorage storage = DrugstoresStorage.getInstance();
        Drugstore drugstore = storage.getDrugstoreById(Id);

        TextView t = (TextView) findViewById(R.id.DrugstoreName);
        t.setText(drugstore.getName());

        t = (TextView) findViewById(R.id.DrugstoreAddress);
        t.setText(drugstore.getCity() + ", " + drugstore.getAddress());
    }
}
