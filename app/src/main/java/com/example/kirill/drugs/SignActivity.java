package com.example.kirill.drugs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.kirill.drugs.Entity.User;
import com.example.kirill.drugs.Storage.UsersStorage;

public class SignActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
    }

    public void SignIn(View view) {
        EditText editText = (EditText) findViewById(R.id.Login);
        String Login = editText.getText().toString();

        EditText editText2 = (EditText) findViewById(R.id.Password);
        String Password = editText2.getText().toString();

        UsersStorage storage = UsersStorage.getInstance();
        User user = storage.getUserByLogin(Login);
        if(user != null)
            if(user.getPassword().equals(Password))
                startActivity(new Intent(this, MainActivity.class));
    }

    public void Registration(View view) {
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }
}
