package com.example.kirill.drugs.Storage;

import com.example.kirill.drugs.Entity.User;

import java.util.ArrayList;

/**
 * Created by Евгений on 20.12.2016.
 */

public class UsersStorage {
    private static UsersStorage instance;
    ArrayList<User> users= null;

    public static synchronized UsersStorage getInstance() {
        if (instance == null) {
            instance = new UsersStorage();
        }
        return instance;
    }

    public UsersStorage() {
        users = new ArrayList<>();
        User admin = new User(1, "Админ", "Админ", "admin", "admin");
        admin.setPower(1);
        users.add(admin);
        User user = new User(2, "Имя", "Фамилия", "user", "user");
        admin.setPower(0);
        users.add(user);
    }

    public User getUserByLogin(String Login)
    {
        for (User x: users) {
            if(x.getLogin().equals(Login))
                return x;
        }
        return null;
    }
}
