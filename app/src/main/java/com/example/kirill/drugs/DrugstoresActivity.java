package com.example.kirill.drugs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView;

import com.example.kirill.drugs.Adapters.DrugstoresAdapter;
import com.example.kirill.drugs.Entity.Drugstore;
import com.example.kirill.drugs.Storage.DrugstoresStorage;

import java.util.ArrayList;

public class DrugstoresActivity extends Activity {
    ArrayList<Drugstore> drugstores = new ArrayList<>();
    DrugstoresAdapter drugstoresAdapter;
    EditText drugstoresFilter;

    /** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drugstores);

        // создаем адаптер
        fillData();
        drugstoresAdapter = new DrugstoresAdapter(this, drugstores);

        // настраиваем список
        ListView ListDrugstores = (ListView) findViewById(R.id.ListDrugstores);
        ListDrugstores.setAdapter(drugstoresAdapter);

        ListDrugstores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                    long id) {
                Intent intent = new Intent(getApplicationContext(), DrugstoreByIdActivity.class);
                intent.putExtra("IdDrugstore", Integer.toString(itemClicked.getId()));
                startActivity(intent);
            }
        });
    }

    // генерируем данные для адаптера
    void fillData() {
        /*for (int i = 1; i <= 20; i++) {
            drugstores.add(new Drugstore(i, "drugstore" + i, "city" + i, "address" + i));
        }*/
        DrugstoresStorage storage = DrugstoresStorage.getInstance();
        drugstores = storage.getDrugstores();
    }

    @Override
    public void onResume(){
        super.onResume();
        drugstoresFilter = (EditText)findViewById(R.id.drugstoresFilter);
        drugstoresFilter.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                DrugstoresStorage storage = DrugstoresStorage.getInstance();
                if (s == null || s.length() == 0) {
                    drugstores = storage.getDrugstores();
                }
                else {
                    drugstores = storage.getDrugstoresWithFilter(s.toString());
                }
                Context c = drugstoresAdapter.getCtx();
                drugstoresAdapter = new DrugstoresAdapter(c, drugstores);
                ListView ListDrugstores = (ListView) findViewById(R.id.ListDrugstores);
                ListDrugstores.setAdapter(drugstoresAdapter);
            }
        });

    }
}
