package com.example.kirill.drugs.Entity;

/**
 * Created by Евгений on 18.12.2016.
 */

public class Drug {
    int Id;
    String Name;
    Double Price;
    String Description;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public Double getPrice() {
        return Price;
    }

    public String getDescription() {
        return Description;
    }

    public Drug(int id, String name, Double price, String description) {

        Id = id;
        Name = name;
        Price = price;
        Description = description;
    }
}
