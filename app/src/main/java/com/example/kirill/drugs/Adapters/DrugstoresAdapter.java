package com.example.kirill.drugs.Adapters;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.kirill.drugs.Entity.Drugstore;
import com.example.kirill.drugs.R;

/**
 * Created by Евгений on 18.12.2016.
 */

public class DrugstoresAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Drugstore> objects;

    public DrugstoresAdapter(Context context, ArrayList<Drugstore> drugstores) {
        ctx = context;
        objects = drugstores;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.drugstores, parent, false);
        }

        Drugstore d = getDrugstore(position);

        // заполняем View в пункте списка данными из товаров: наименование, цена
        // и картинка
        view.setId(d.getId());
        ((TextView) view.findViewById(R.id.Description)).setText(d.getName());
        ((TextView) view.findViewById(R.id.City)).setText(d.getCity() + "");
        return view;
    }

    // товар по позиции
    Drugstore getDrugstore(int position) {
        return ((Drugstore) getItem(position));
    }

    public Context getCtx() {
        return ctx;
    }
}
