package com.example.kirill.drugs.Entity;

/**
 * Created by Евгений on 18.12.2016.
 */

public class Drugstore {
    int Id;
    String Name;
    String City;
    String Address;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public String getCity() {
        return City;
    }

    public String getAddress() {
        return Address;
    }

    public Drugstore(int id, String name, String city, String address) {
        Id = id;
        Name = name;
        City = city;
        Address = address;
    }
}
