package com.example.kirill.drugs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import java.util.Timer;
import java.util.TimerTask;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        new Timer().schedule(new TimerTask(){
            public void run() {
                startActivity(new Intent(Main2Activity.this, SignActivity.class));
                Intent intent = new Intent(Main2Activity.this,
                        SignActivity.class);
                startActivity(intent);
                finish();
            }
        }, 4000);

    }
}
