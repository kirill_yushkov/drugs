package com.example.kirill.drugs.Storage;

import android.widget.ListView;

import com.example.kirill.drugs.Entity.Drug;
import com.example.kirill.drugs.Entity.Keys;

import java.util.ArrayList;

/**
 * Created by Евгений on 18.12.2016.
 */

public class DrugsStorage {
    public ArrayList<Drug> getDrugs() {
        return drugs;
    }

    private static DrugsStorage instance;
    ArrayList<Drug> drugs= null;
    ArrayList<Keys> Ids= null;
    private DrugsStorage ()
    {
        Ids = new ArrayList<>();
        ArrayList<Integer> buf1 = new ArrayList<>();
        drugs = new ArrayList<>();
        drugs.add(new Drug(1, "Аспирин", 0.55, "От головы"));
        buf1.add(1); buf1.add(2); buf1.add(4); buf1.add(8);
        Ids.add(new Keys(1,buf1));
        ArrayList<Integer> buf2 = new ArrayList<>();
        drugs.add(new Drug(2, "Ибуфен", 4.9, "Жаропонижающее, противовоспалительное"));
        buf2.add(3); buf2.add(5); buf2.add(6); buf2.add(7); buf2.add(9); buf2.add(11);
        Ids.add(new Keys(2,buf2));
        ArrayList<Integer> buf3 = new ArrayList<>();
        drugs.add(new Drug(3, "Активированный уголь", 0.4, "От живота"));
        buf3.add(1); buf3.add(2); buf3.add(3); buf3.add(7); buf3.add(9); buf3.add(10);
        Ids.add(new Keys(3,buf3));
        ArrayList<Integer> buf4 = new ArrayList<>();
        drugs.add(new Drug(4, "Феназипам", 6.3, "Успокаивающее"));
        buf4.add(2); buf4.add(4); buf4.add(6); buf4.add(7); buf4.add(8); buf4.add(10);
        Ids.add(new Keys(4,buf4));
        ArrayList<Integer> buf5 = new ArrayList<>();
        drugs.add(new Drug(5, "Афобазол", 12.5, "Успокаивающее"));
        buf5.add(1); buf5.add(2); buf5.add(5); buf5.add(6);
        Ids.add(new Keys(5,buf5));
        ArrayList<Integer> buf6 = new ArrayList<>();
        drugs.add(new Drug(6, "Мезим", 2.6, "От живота"));
        buf6.add(7); buf6.add(8);
        Ids.add(new Keys(6,buf6));
        ArrayList<Integer> buf7 = new ArrayList<>();
        drugs.add(new Drug(7, "Спазмалгон", 2.4, "Обезбаливающее"));
        buf7.add(1); buf7.add(2); buf7.add(3); buf7.add(4); buf7.add(8); buf7.add(11);
        Ids.add(new Keys(7,buf7));
        ArrayList<Integer> buf8 = new ArrayList<>();
        drugs.add(new Drug(8, "Кетонов", 3.7, "Обезбаливающее"));
        buf8.add(7); buf8.add(9); buf8.add(10);
        Ids.add(new Keys(8,buf8));
        ArrayList<Integer> buf9 = new ArrayList<>();
        drugs.add(new Drug(9, "Анальгин", 0.8, "Обезбаливающее"));
        buf9.add(2); buf9.add(3); buf9.add(5); buf9.add(10); buf9.add(11);
        Ids.add(new Keys(9,buf9));
        ArrayList<Integer> buf10 = new ArrayList<>();
        drugs.add(new Drug(10, "Йод", 0.9, "Обеззараживающее"));
        buf10.add(1); buf10.add(2); buf10.add(5); buf10.add(6); buf10.add(7); buf10.add(10);
        Ids.add(new Keys(10,buf10));
        ArrayList<Integer> buf11 = new ArrayList<>();
        drugs.add(new Drug(11, "Contex", 5.4, "Контрацептив"));
        buf11.add(1); buf11.add(2); buf11.add(3); buf11.add(4); buf11.add(5); buf11.add(6);
        buf11.add(7); buf11.add(8); buf11.add(9); buf11.add(10); buf11.add(11);
        Ids.add(new Keys(11,buf11));
        ArrayList<Integer> buf12 = new ArrayList<>();
        drugs.add(new Drug(12, "Ибуклин", 6.1, "Жаропонижающее, болеутоляющее"));
        buf12.add(6); buf12.add(7); buf12.add(8); buf12.add(10); buf12.add(11);
        Ids.add(new Keys(12,buf12));
        ArrayList<Integer> buf13 = new ArrayList<>();
        drugs.add(new Drug(13, "Парацетамол", 0.9, "Жаропонижающее"));
        buf13.add(1); buf13.add(3); buf13.add(5); buf13.add(8); buf13.add(9); buf13.add(10);
        Ids.add(new Keys(13,buf13));
    }

    public static synchronized DrugsStorage getInstance() {
        if (instance == null) {
            instance = new DrugsStorage();
        }
        return instance;
    }

    public ArrayList<Drug> getDrugsWithFilter(String str) {
        ArrayList<Drug> buf = new ArrayList<>();
        for (int i = 0; i < drugs.size(); i++)
            if(drugs.get(i).getName().indexOf(str) != -1 || drugs.get(i).getDescription().indexOf(str) != -1)
                buf.add(drugs.get(i));
        return buf;
    }

    public Drug getDrugById(int Id) {
        for (Drug x: drugs) {
            if(x.getId() == Id)
                return x;
        }
        return null;
    }


    public Keys getIds(int Id)
    {
        for (Keys x: Ids) {
            if(x.getId() == Id)
                return x;
        }
        return null;
    }



}
