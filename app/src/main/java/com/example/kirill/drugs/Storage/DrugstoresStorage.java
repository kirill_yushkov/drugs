package com.example.kirill.drugs.Storage;

import com.example.kirill.drugs.Entity.Drugstore;
import com.example.kirill.drugs.Entity.Keys;

import java.util.ArrayList;

/**
 * Created by Евгений on 18.12.2016.
 */

public class DrugstoresStorage {
    private static DrugstoresStorage instance;
    ArrayList<Drugstore> drugstores = null;
    private DrugstoresStorage ()
    {
        drugstores = new ArrayList<>();
        drugstores.add(new Drugstore(1, "Айболит", "Витебск", "Чапаева, д.24"));
        drugstores.add(new Drugstore(2, "Лекарь", "Минск", "Пр-т Машерова, д.33"));
        drugstores.add(new Drugstore(3, "Здорово", "Могилев", "Пр-т Мира, д.29"));
        drugstores.add(new Drugstore(4, "Не болей-ка", "Гродно", "Ленина, д.17"));
        drugstores.add(new Drugstore(5, "Лекарь", "Минск", "Пр-т Машерова, д.33"));
        drugstores.add(new Drugstore(6, "Здоровяк", "Могилев", "Пр-т Мира, д.29"));
        drugstores.add(new Drugstore(7, "Жив-здоров", "Гомель", "Привокзальная, д"));
        drugstores.add(new Drugstore(8, "Аптека №37", "Брест", "Бресткая, д.52"));
        drugstores.add(new Drugstore(9, "Тон-тон", "Витебск", "Московская, д.14"));
        drugstores.add(new Drugstore(10, "Дзинь-дзинь", "Витебск", "Мира, д.10"));
        drugstores.add(new Drugstore(11, "24/7", "Витебск", "Пр-т Победы"));
    }

    public static synchronized DrugstoresStorage getInstance() {
        if (instance == null) {
            instance = new DrugstoresStorage();
        }
        return instance;
    }

    public ArrayList<Drugstore> getDrugstores() {
        return drugstores;
    }

    public ArrayList<Drugstore> getDrugstoresWithFilter(String str) {
        ArrayList<Drugstore> buf = new ArrayList<>();
        for (int i = 0; i < drugstores.size(); i++)
            if(drugstores.get(i).getName().indexOf(str) != -1 || drugstores.get(i).getAddress().indexOf(str) != -1 || drugstores.get(i).getCity().indexOf(str) != -1)
                buf.add(drugstores.get(i));
        return buf;
    }

    public Drugstore getDrugstoreById(int Id) {
        for (Drugstore x: drugstores) {
            if(x.getId() == Id)
                return x;
        }
        return null;
    }

    public ArrayList<Drugstore> getDrugstoresByIds(Keys ids)
    {
        ArrayList<Drugstore> arr = new ArrayList<>();
        for (Integer x:ids.getIds()) {
            try {
                if (drugstores.get(x.intValue()) != null)
                    arr.add(drugstores.get(x.intValue()));
            }catch (Exception e)
            {}
        }
        return arr;
    }
}
