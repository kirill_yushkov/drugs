package com.example.kirill.drugs.Entity;

import java.util.ArrayList;

/**
 * Created by Евгений on 20.12.2016.
 */

public class Keys {
    int Id;
    ArrayList<Integer> Ids;

    public Keys(int id, ArrayList<Integer> ids) {
        Id = id;
        Ids = ids;
    }

    public ArrayList<Integer> getIds() {
        return Ids;
    }

    public int getId() {

        return Id;
    }
}
