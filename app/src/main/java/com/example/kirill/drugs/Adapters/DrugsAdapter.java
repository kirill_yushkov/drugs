package com.example.kirill.drugs.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.kirill.drugs.Entity.Drug;
import com.example.kirill.drugs.Entity.Drugstore;
import com.example.kirill.drugs.R;

import java.util.ArrayList;

/**
 * Created by Евгений on 18.12.2016.
 */

public class DrugsAdapter  extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Drug> objects;

    public Context getCtx() {
        return ctx;
    }

    public DrugsAdapter(Context context, ArrayList<Drug> drugs) {
        ctx = context;
        objects = drugs;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.drugs, parent, false);
        }

        Drug d = getDrug(position);

        // заполняем View в пункте списка данными из товаров: наименование, цена
        // и картинка

        view.setId(d.getId());
        ((TextView) view.findViewById(R.id.Name)).setText(d.getName());
        ((TextView) view.findViewById(R.id.Price)).setText(d.getPrice() + " BYN");
        return view;
    }

    // товар по позиции
    Drug getDrug(int position) {
        return ((Drug) getItem(position));
    }
}

