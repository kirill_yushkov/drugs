package com.example.kirill.drugs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.kirill.drugs.Adapters.DrugsAdapter;
import com.example.kirill.drugs.Entity.Drug;
import com.example.kirill.drugs.Storage.DrugsStorage;

import java.util.ArrayList;

public class MainActivity extends Activity {
    ArrayList<Drug> drugs = new ArrayList<Drug>();
    DrugsAdapter drugsAdapter;
    EditText drugsFilter;

    /** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // создаем адаптер
        fillData();
        drugsAdapter = new DrugsAdapter(this, drugs);

        // настраиваем список
        ListView ListDrugs = (ListView) findViewById(R.id.ListDrugs);
        ListDrugs.setAdapter(drugsAdapter);

        ListDrugs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                    long id) {
                Intent intent = new Intent(getApplicationContext(),DrugByIdActivity.class);
                intent.putExtra("IdDrug", Integer.toString(itemClicked.getId()));
                startActivity(intent);
            }
        });


    }

    // генерируем данные для адаптера
    void fillData() {
        /*for (int i = 1; i <= 20; i++) {
            drugs.add(new Drug(i,"drug"+i,1000+i*120.2,"description" + i));
        }*/
        DrugsStorage storage = DrugsStorage.getInstance();
        drugs = storage.getDrugs();
    }

    public void ToDrugstores(View view)
    {
        Intent intent = new Intent(this, DrugstoresActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume(){
        super.onResume();
        drugsFilter = (EditText)findViewById(R.id.drugsFilter);
        drugsFilter.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                DrugsStorage storage = DrugsStorage.getInstance();
                if (s == null || s.length() == 0) {
                    drugs = storage.getDrugs();
                }
                else {
                    drugs = storage.getDrugsWithFilter(s.toString());
                }
                Context c = drugsAdapter.getCtx();
                drugsAdapter = new DrugsAdapter(c, drugs);
                ListView ListDrugs = (ListView) findViewById(R.id.ListDrugs);
                ListDrugs.setAdapter(drugsAdapter);
            }
        });

    }


}
