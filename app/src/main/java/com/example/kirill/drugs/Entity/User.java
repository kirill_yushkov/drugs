package com.example.kirill.drugs.Entity;

/**
 * Created by Евгений on 20.12.2016.
 */

public class User {
    int Id;
    String FName, LName;
    String Login, Password;
    int Power;

    public User(int id, String FName, String LName, String login, String password) {

        Id = id;
        this.FName = FName;
        this.LName = LName;
        Login = login;
        Password = password;
    }

    public int getId() {
        return Id;
    }

    public String getFName() {
        return FName;
    }

    public String getLName() {
        return LName;
    }

    public String getLogin() {
        return Login;
    }

    public String getPassword() {
        return Password;
    }

    public int getPower() {
        return Power;
    }

    public void setPower(int power) {
        Power = power;
    }
}
