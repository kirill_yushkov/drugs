package com.example.kirill.drugs;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.kirill.drugs.Adapters.DrugstoresAdapter;
import com.example.kirill.drugs.Entity.Drug;
import com.example.kirill.drugs.Entity.Drugstore;
import com.example.kirill.drugs.Entity.Keys;
import com.example.kirill.drugs.Storage.DrugsStorage;
import com.example.kirill.drugs.Storage.DrugstoresStorage;

import java.security.Key;
import java.util.ArrayList;

public class DrugByIdActivity extends AppCompatActivity {
    ArrayList<Drugstore> drugstores = new ArrayList<>();
    DrugstoresAdapter drugstoresAdapter;
    EditText drugstoresFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drug_by_id);

        Intent intent = getIntent();

        int Id = Integer.parseInt(intent.getStringExtra("IdDrug"));

        DrugsStorage storage = DrugsStorage.getInstance();
        Drug drug = storage.getDrugById(Id);
        TextView t = (TextView) findViewById(R.id.DrugName);
        t.setText(drug.getName());

        t = (TextView) findViewById(R.id.DrugPrice);
        t.setText("Стоимость: " + drug.getPrice() + " BYN");

        t = (TextView) findViewById(R.id.DrugDescription);
        t.setText(drug.getDescription());


        fillData(Id);
        drugstoresAdapter = new DrugstoresAdapter(this, drugstores);

        // настраиваем список
        ListView ListDrugstores = (ListView) findViewById(R.id.ListDrugstores);
        ListDrugstores.setAdapter(drugstoresAdapter);

        ListDrugstores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                    long id) {
                Intent intent = new Intent(getApplicationContext(), DrugstoreByIdActivity.class);
                intent.putExtra("IdDrugstore", Integer.toString(itemClicked.getId()));
                startActivity(intent);
            }
        });
    }

    // генерируем данные для адаптера
    void fillData(int id) {
        /*for (int i = 1; i <= 20; i++) {
            drugstores.add(new Drugstore(i, "drugstore" + i, "city" + i, "address" + i));
        }*/
        DrugsStorage storageDrugs = DrugsStorage.getInstance();
        Keys ids = storageDrugs.getIds(id);

        DrugstoresStorage storage = DrugstoresStorage.getInstance();
        drugstores = storage.getDrugstoresByIds(ids);
    }

    @Override
    public void onResume(){
        super.onResume();
        drugstoresFilter = (EditText)findViewById(R.id.drugstoresFilter);
        drugstoresFilter.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                DrugstoresStorage storage = DrugstoresStorage.getInstance();
                if (s == null || s.length() == 0) {
                    drugstores = storage.getDrugstores();
                }
                else {
                    drugstores = storage.getDrugstoresWithFilter(s.toString());
                }
                Context c = drugstoresAdapter.getCtx();
                drugstoresAdapter = new DrugstoresAdapter(c, drugstores);
                ListView ListDrugstores = (ListView) findViewById(R.id.ListDrugstores);
                ListDrugstores.setAdapter(drugstoresAdapter);
            }
        });
    }
}
